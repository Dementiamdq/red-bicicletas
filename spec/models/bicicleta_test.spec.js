var mongoose = require('mongoose');

const { request } = require("express");

var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function(){
    beforeAll(function(done) {
        mongoose.connection.close().then(() => {
            var mongoDB = 'mongodb://localhost/testdb';
            mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
            mongoose.set('useCreateIndex', true);
            var db = mongoose.connection;
            db.on('error', console.error.bind(console, 'MongoDB error de conexión: '));
            db.once('open', function () {
                console.log('Estamos conectados a la base DB test!');
                done();
            });
        });
        
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);

            done();
        })
    })

    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', () => {
            var bici= Bicicleta.createInstance(1, "verde", "urbana", [-39,-59]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe=("verde");
            expect(bici.modelo).toBe=("urbana");
            expect(bici.ubicacion[0]).toBe=(-39);
            expect(bici.ubicacion[1]).toBe=(-59);
        })
    })

    describe('Bicicleta.allBicis', () => {
        it('Comienza vacia', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                if(err) console.log(err);
                expect(bicis.length).toBe(0);
                done();
            })
        })
    })

    describe('Bicicleta.add', () => {
        it('Agrega una bici', (done) => {
            var aBici = new Bicicleta({code: 1, color: "negro", modelo: "carrera"});
            Bicicleta.add(aBici, function(err, newBici){
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);

                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('Debe devolver la bicicleta con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                
                var aBici = new Bicicleta({code: 1, color: "violeta", modelo: "BMX"});
                Bicicleta.add(aBici, function(err, newBici){
                    if (err) console.log(err);

                    var aBici2 = new Bicicleta({code: 2, color: "negro", modelo: "carrera"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function(err, targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);

                            done();
                        })
                    })
                })
            })
        })
    })

});


/*
beforeEach(() => {Bicicleta.allBicis = []});
describe('Bicicleta.allBicis', () => {
    it('Comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    })
})

 describe('Bicicleta.add', () => {
    it('Agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        
        var a = new Bicicleta(1, 'rojo', 'urbana', [-37.96, -57.56]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    })
})

describe('Bicicleta.findById', () => {
    it('Debe devolver la bici con ID 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        
        var a = new Bicicleta(1, 'rojo', 'urbana', [-37.96, -57.56]);
        Bicicleta.add(a);
        var b = new Bicicleta(2, 'verde', 'carrera', [-37.26, -57.16]);
        Bicicleta.add(b);

        var biciBuscada = Bicicleta.findById(1);
        expect(biciBuscada).toBe(a);
    })
})

describe('Bicicleta.removeById', () => {
    it('Debe devolver una coleccion con una sola bicicleta', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        
        var a = new Bicicleta(1, 'rojo', 'urbana', [-37.96, -57.56]);
        Bicicleta.add(a);
        

        Bicicleta.removeById(1);
        expect(Bicicleta.allBicis.length).toBe(1);
    })
})
 */

