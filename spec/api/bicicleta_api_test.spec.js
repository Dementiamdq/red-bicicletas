var mongoose = require('mongoose')
const Bicicleta = require("../../models/bicicleta")
var request = require('request')
//var server = require('../../bin/www')
var base_url = 'http://localhost:3000/api/bicicletas'

describe('Bicicleta API', function(){       
    beforeAll(function(done) {
        mongoose.connection.close().then(() => {
            var mongoDB = 'mongodb://localhost/testdb';
            mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
            mongoose.set('useCreateIndex', true);
            var db = mongoose.connection;
            db.on('error', console.error.bind(console, 'MongoDB error de conexión: '));
            db.once('open', function () {
                console.log('Estamos conectados a la base DB test!');
                done();
            });
        });
        
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);

            done();
        })
    })    
    

    describe('GET BICICLETAS /', () => {
        it('status 200',(done) => {
            request.get(base_url, function (error, response, body) {
                expect(response.statusCode).toBe(200)
                let result = JSON.parse(body)
                console.log(result)              
                expect(result.bicis.length).toBe(0)      
                done()
            })
        })
    })

    describe('POST Bicicleta API /create', () => {
        it('status 200', (done) => {
            var headers = {'content-type' : 'application/json'}
            var aBici = '{"id":10,"color":"violeta","modelo":"montaña","latitud":-39,"longitud":-58}'
            request.post({
                headers: headers,
                url: base_url+'/create',
                body: aBici
            }, function(error,response,body){
                console.log(body);
                expect(response.statusCode).toBe(200)
                var result = JSON.parse(body)
                console.log(result.bicicleta);
                expect(result.bicicleta.color).toBe('violeta')
                expect(result.bicicleta.ubicacion[0]).toBe(-39)
                expect(result.bicicleta.ubicacion[1]).toBe(-58)
                done()
            })
        })
    })

    describe('POST Bicicleta APi /delete', () =>{
        it('status 204', (done) => {
            var headers = {'content-type' : 'application/json'}
            var aBici = '{"id":15,"color":"amarillo","modelo":"urbana","latitud":-39,"longitud":-58}'            
            request.post({
                headers: headers,
                url: base_url+'/create',
                body: aBici
            },function(error,response,body){
                expect(response.statusCode).toBe(200)
                if (error) console.log(error)
                let aDel ='{"id":15}'
                request.delete({
                    headers: headers,
                    url: base_url+'/delete',
                    body: aDel
                },function(error,response,body){
                    expect(response.statusCode).toBe(204)
                    if(error) console.log(error)
                    Bicicleta.allBicis(function (err,bicis){
                        expect(bicis.length).toBe(0)
                        if (err) console.log(err)
                        done()
                    })
                })
            })
        })
    })


})


/* ar Bicicleta = require('../../models/bicicleta');
var request = require('request');
//var server = require('../../bin/www');

beforeEach(() => {Bicicleta.allBicis = []});
describe('Bicicleta API', () => {
    describe('GET BICICLETAS /', () => {
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'rojo', 'urbana', [-37.96, -57.56]);
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas', function(error, response, body){
                expect(response.statusCode).toBe(200);
            })
        })
    });
});

describe('POST Bicicleta API /create', () => {
    it('Status 200', (done) => {
        var headers = {'content-type' : 'application/json'};
        var aBici = '{"id": 10, "color": "rojo", "modelo": "urbana", "latitud": -37, "longitud": -57}';
        request.post({
            headers: headers,
            url:    'http://localhost:3000/api/bicicletas/create',
            body: aBici
        }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("rojo");
                done();
        });
    });
});

describe('POST Bicicleta API /delete', () => {
    it('Status 200', (done) => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(11, 'rojo', 'urbana', [-37.96, -57.56]);
        Bicicleta.add(a);
        a = new Bicicleta(12, 'azul', 'carrera', [-38.96, -58.56]);
        Bicicleta.add(a);

        var headers = {'content-type' : 'application/json'};
        var aBici = '{"id": 11}';
        request.delete({
            headers: headers,
            url:    'http://localhost:3000/api/bicicletas/delete',
            body: aBici
        }, function(error, response, body){
                expect(response.statusCode).toBe(204);
                expect(Bicicleta.allBicis.length).toBe(1);
                done();
        });
    });
});

describe('POST Bicicleta API /update', () => {
    it('Status 200', (done) => {
        var a = new Bicicleta(13, 'violeta', 'montaña', [-37.96, -57.56]);
        Bicicleta.add(a);

        var headers = {'content-type' : 'application/json'};
        var aBici = '{"id": 13, "color": "amarillo", "modelo": "urbana", "latitud": -37, "longitud": -57}';
        request.post({
            headers: headers,
            url:    'http://localhost:3000/api/bicicletas/update',
            body: aBici
        }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(13).color).toBe("amarillo");
                expect(Bicicleta.findById(13).modelo).toBe("urbana");
                done();
        });
    });
}); */