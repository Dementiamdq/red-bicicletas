var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res){
    Bicicleta.allBicis(function(err, bicis){
        res.status(200).json({bicis});
    });
}

exports.bicicleta_create = function(req, res) {
    var bici = new Bicicleta({
        code: req.body.id,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.latitud, req.body.longitud]
    });

    Bicicleta.add(bici, (err, newBici) => {
        if (err){res.status(500)}
        else{
        res.status(200).json({
            bicicleta: newBici
        });
        }
    });
 /*    Bicicleta.add(bici);

    res.status(200).json({
        bicicleta: bici
    }); */
}

exports.bicicleta_delete = function(req, res){
    console.log(req.body.id);
    Bicicleta.removeByCode(req.body.id, function(err){
        res.status(204).send();
    });
}

exports.bicicleta_update = function(req, res){
    console.log(req.body);
    Bicicleta.findByCode(req.body.id, function(err, bici){
        console.log(bici);
        bici.color = req.body.color;
        bici.modelo = req.body.modelo;
        bici.ubicacion[0] = req.body.latitud;
        bici.ubicacion[1] = req.body.longitud;
        bici.save();
        res.status(200).json({
            bicicleta: bici
        });
    });
    
}

