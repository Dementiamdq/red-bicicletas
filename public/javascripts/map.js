var map = L.map(main_map).setView([-37.9681676,-57.5634636], 12);

L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
    maxZoom: 18
    }).addTo(map);

//L.marker([-37.96, -57.56],{draggable: true}).addTo(map);
//L.marker([-37.9950,-57.5537],{draggable: true}).addTo(map);
//L.marker([-38.0053,-57.5943],{draggable: true}).addTo(map);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion,{title: bici.id}).addTo(map);
        }); 
    }
})


